import aedat
from typing import Any, Callable, cast, Dict, List, Optional, Tuple
import struct
import numpy as np
import glob, os
import pandas as pd
import csv
def load_aedat_v3(file_name: str) -> Dict:
    '''
    :param file_name: path of the aedat v3 file
    :type file_name: str
    :return: a dict whose keys are ``['t', 'x', 'y', 'p']`` and values are ``numpy.ndarray``
    :rtype: Dict
    This function is written by referring to https://gitlab.com/inivation/dv/dv-python . It can be used for DVS128 Gesture.
    '''
    with open(file_name, 'rb') as bin_f:
        # skip ascii header
        line = bin_f.readline()
        while line.startswith(b'#'):
            if line == b'#!END-HEADER\r\n':
                break
            else:
                line = bin_f.readline()

        txyp = {
            't': [],
            'x': [],
            'y': [],
            'p': [],
            'mapping' : []
        }
        while True:
            header = bin_f.read(28)
            if not header or len(header) == 0:
                break

            # read header
            e_type = struct.unpack('H', header[0:2])[0]
            e_source = struct.unpack('H', header[2:4])[0]
            e_size = struct.unpack('I', header[4:8])[0]
            e_offset = struct.unpack('I', header[8:12])[0]
            e_tsoverflow = struct.unpack('I', header[12:16])[0]
            e_capacity = struct.unpack('I', header[16:20])[0]
            e_number = struct.unpack('I', header[20:24])[0]
            e_valid = struct.unpack('I', header[24:28])[0]

            data_length = e_capacity * e_size
            data = bin_f.read(data_length)
            counter = 0
            with open(file_name[:-6]+"_labels.csv") as csv_file:
                if e_type == 1:
                    while data[counter:counter + e_size]:
                        aer_data = struct.unpack('I', data[counter:counter + 4])[0]
                        timestamp = struct.unpack('I', data[counter + 4:counter + 8])[0] | e_tsoverflow << 31
                        x = (aer_data >> 17) & 0x00007FFF
                        y = (aer_data >> 2) & 0x00007FFF
                        pol = (aer_data >> 1) & 0x00000001
                        counter = counter + e_size
                        
                        txyp['x'].append(x)
                        txyp['y'].append(y)
                        txyp['t'].append(timestamp)
                        txyp['p'].append(pol)
                        timestamp_found = False
                        csv_file.seek(0)
                        mapping = csv.reader(csv_file)
                        next(mapping)
                        for line in mapping:
                            if(timestamp >= int(line[1]) and timestamp <= int(line[2])):
                                txyp['mapping'].append(line[0])
                                timestamp_found = True
                                break
                        if not timestamp_found:
                            txyp['mapping'].append("-1")
                        
                else:
                    # non-polarity event packet, not implemented
                    pass
        
        txyp['x'] = np.asarray(txyp['x'])
        txyp['y'] = np.asarray(txyp['y'])
        txyp['t'] = np.asarray(txyp['t'])
        txyp['p'] = np.asarray(txyp['p'])
        return txyp



os.chdir("DvsGesture/")

for input_file in glob.glob("*lab.aedat"):
    print(input_file)
    df = pd.DataFrame(load_aedat_v3(input_file))
    df.to_csv(input_file[:-6]+"_to_mapping.csv")
    print(df)
    





